# POGNAKEVAS

**PO**st**G**res **NA**ive **KE**y **VA**lue **S**tore

## Usage

1. Start POGNAKEVAS server

```
./pognakevas start
```

2. Run CLI

```
./pognakevas cli
```

3. Create entries

```
SELECT POGNAKEVAS_SET('key1', 'value1');
SELECT POGNAKEVAS_SET('key2', 'value2');
SELECT POGNAKEVAS_SET('key3', 'value2');
```

4. Get the value for 'key1'

```
SELECT POGNAKEVAS_GET('key1');

 pognakevas_get
----------------
 value1
```

5. Set a 30s timeout on 'key1'

```
SELECT POGNAKEVAS_EXPIRE('key1', 30);
```

6. Get the 'key1' expiration time

```
SELECT POGNAKEVAS_EXPIRETIME('key1');


   pognakevas_expiretime
----------------------------
 2021-06-19 21:31:50.713633
```

7. Verify if 'key1' has expired

```
SELECT POGNAKEVAS_EXPIRED('key1');

 pognakevas_expired
--------------------
 f
```

8. Wait for about 30 seconds...

```
SELECT POGNAKEVAS_GET('key1');

 pognakevas_get
----------------

pognakevas=# SELECT POGNAKEVAS_EXPIRED('key1');

 pognakevas_expired
--------------------
 t
```

## TODO

- [ ] Create a NodeJS client
- [ ] Create a Golang client
- [ ] Create a Rust client
- [ ] Create a Ruby client
- [ ] Create a Python client
- [ ] Create a PHP client
- [ ] Create a ... client

## License

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE<br />
Version 2, December 2004

Copyright (C) 2021 Gregoire Lejeune &lt;gregoire.lejeune@free.fr&gt;

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE<br />
TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

0. You just DO WHAT THE FUCK YOU WANT TO.
