#!/bin/sh

source pognakevas.conf

POGNAKEVAS_INIT_DEST="postgres://${POGNAKEVAS_USERNAME}:${POGNAKEVAS_PASSWORD}@pognakevas-server/pognakevas"

echo "Init pognakevas database..."
NB_POGNAKEVAS_TABLES=$(psql "$POGNAKEVAS_INIT_DEST" -c "\dt" -A 2>/dev/null | wc -l)

if [ "$NB_POGNAKEVAS_TABLES" = "0" ] || [ -n "$FORCE_POGNAKEVAS_INIT" ] ; then
  echo "Dump database..."
  cat pognakevas.sql  | psql "$POGNAKEVAS_INIT_DEST"
else 
  echo "Database already exist. To force, add FORCE_POGNAKEVAS_INIT=1 in pognakevas.sql"
fi


echo "Bye folks!"
